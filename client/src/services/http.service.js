import { Alert } from 'react-native';
import { storeTokenInfo } from './asyncStorage.service';
import { getTokenInfo } from './asyncStorage.service';
import { validationSchemaLogin } from '../validation/validationLogin';
import { validationSchemaRegistration } from '../validation/validationRegistration';

const URL = 'http://192.168.1.132:3000';

export async function registration(data) {
    try{
        await validationSchemaRegistration.validate(data)
        const res = await fetch(`${URL}/auth/registration`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        if (res.status == 200) {
            return true
        } else {
            Alert.alert('Error', 'Something went wrong... Server is not answering!!!');
            return false;
        }
    }catch(e){
        Alert.alert(e.message)
    }
   
}

export async function logIn(data) {
    try{
        await validationSchemaLogin.validate(data)
        const res = await fetch(`${URL}/auth/login`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }); 
        if (res.status == 200) {
            const json = await res.json();
            storeTokenInfo({
                accessToken: json.token,
            });
    
            return json; 
        }
        return false
    }catch (e) {
        Alert.alert(e.message)
    }
   
}

export async function getAllProducts() {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/product/getAll`, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`
        }
    });
    const json = await res.json();
    return json;
}

export async function addProduct(id) {
    const tokenInfo = await getTokenInfo();
    if (tokenInfo && typeof tokenInfo !== 'boolean') {
        const res = await fetch(`${URL}/cart/addProducts`, {
            method: "POST",
            body: JSON.stringify(id),
            headers: {
                'Authorization': `Bearer ${tokenInfo.accessToken}`,
                'Content-Type': 'application/json'
            }
        });
        const json = await res.json();
        return json;
    }
    return false
} 

export async function getCartById() {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/cart/getbyId`, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}

export async function getAllOrders() {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/order/getAll`, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json
}

export async function createOrder() {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/order/create`, {
        method: "POST",
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}




