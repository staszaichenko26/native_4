import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { getAllOrders } from '../services/http.service';


export const Order = ({ route }) => {

    const [orders, setOrders] = useState([]);

    useEffect(() => {
        (async () =>{
            const result = await getAllOrders();
            setOrders(result)
        })()
    }, [route.params])

  

    return (
        <View style={styles.view}>
            <ScrollView>
            <Text>
                    {
                       orders.map(order => (
                            <View key={order._id}>
                                <Text>Order's ID: {order._id}</Text>
                            </View>
                        ))
                    }
                </Text>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});


