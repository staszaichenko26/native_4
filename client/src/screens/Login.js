import React, { useState, useContext } from 'react';
import { Text, TouchableHighlight, StyleSheet, TextInput, Button, View, Alert } from 'react-native';
import { logIn } from '../services/http.service';
import Context from '../utils/context';

export const Login = ({ navigation }) => {

  const context = useContext(Context);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  async function loginBtn() {
    if (email === '' || password === '') {
      return Alert.alert(`Fields shouldn't be empty!`)
    }

    const res = await logIn({ email, password });
    
    if (res) {
      context.handleSetData({ email, password})
      navigation.navigate('Main')
    }
    else{
      Alert.alert("Incorrect email or password!")
    }
  }

  return (
    <View style={styles.view} >
      <TextInput placeholder="Email"
        value={email}
        onChangeText={text => setEmail(text)}
        style={styles.input} />
      <TextInput placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
        style={styles.input} />

      <Button title="Log in" onPress={loginBtn} />
      <TouchableHighlight onPress={() => navigation.navigate('Registration')} >
        <Text>Registration</Text>
      </TouchableHighlight>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  input: {
    height: 40,
    marginBottom: 5,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1,
    padding: 5
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
});


