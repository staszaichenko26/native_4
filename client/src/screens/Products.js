import React, { useEffect, useState, useContext } from 'react';
import { Alert, Button, ScrollView, StyleSheet, Text, View } from 'react-native';
import { getAllProducts } from '../services/http.service';
import { addProduct } from '../services/http.service';
import Context from '../utils/context';

export const Products = () => {
  const context = useContext(Context);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    (async () => {
      const productsArr = await getAllProducts();
      setProducts(productsArr);
      const productsLength = products.length
      context.handleSetData({productsLength});
    })()
  }, [])

  const addToCartBtn = (id) => {
    addProduct({ id })
    Alert.alert(`This product added to your cart!`)

  }


  return (
    <View style={styles.view}>
      <ScrollView style={styles.scroll}>
        <View  style={styles.products}>
          {
            products.map(product => (
              <View key={product._id} >
                <Text style={styles.productName}>{product.name}</Text>
                <Text>Price: {product.price} $</Text>
                <Button title="Add to cart" onPress={() => addToCartBtn(product._id)} />
              </View>
            ))
          }
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  productName: {
    fontSize: 25,
    fontWeight: 'bold'
  },
  products: {
    flexDirection: 'column'
  },
  scroll: {
    width: "80%"
  }
});


