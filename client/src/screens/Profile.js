import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const Profile = () => {

    return (
        <View style={styles.view}>
            <Text>Profile</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});


