import { SHOW_INFO } from "../constants/user.constant";
export const defaultState = {
    productsLength:0
}

export const ReducerFunction = (state = defaultState, action) => {
    switch (action.type) {
        case SHOW_INFO:
            let newState = {
                ...state, ...action.data
            };
            return newState;
        default:
            return state;
    }

};
