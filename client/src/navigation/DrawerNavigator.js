import React from "react";
import {Button} from 'react-native';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Products } from "../screens/Products";
import { Cart } from "../screens/Cart";
import BottomTabNavigator from './TabNavigator'

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Main" component={BottomTabNavigator} options={({navigation, route}) => ({ headerRight: () => (
        <Button onPress={() => navigation.navigate('Login')} title="Logout" />
      )})}/>
      <Drawer.Screen name="Products" component={Products} />
      <Drawer.Screen name="Cart" component={Cart} />
    </Drawer.Navigator>
  );
}

export default DrawerNavigator;