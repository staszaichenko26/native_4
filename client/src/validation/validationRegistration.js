import * as Yup from 'yup'

export const validationSchemaRegistration = Yup.object().shape({
    name: Yup.string()
        .label('Name')
        .required('Please enter your name')
        .min(4, 'Name must have at least 4 characters '),
    surname: Yup.string()
        .label('Surname')
        .required('Please enter your surname')
        .min(4, 'Surname must have at least 4 characters '),
    email: Yup.string()
        .label('Email')
        // .email('Enter a valid email')
        .required('Please enter your email address'),
    password: Yup.string()
        .label('Password')
        .required('Please enter your password')
        .min(4, 'Password must have at least 4 characters ')
})