import * as Yup from 'yup'

export const validationSchemaLogin = Yup.object().shape({
    email: Yup.string()
        .label('Email')
        .required('Please enter your email address')
        .min(4, 'Password must have at least 4 characters '),
    password: Yup.string()
        .label('Password')
        .required('Please enter your password')
        .min(4, 'Password must have at least 4 characters ')
})