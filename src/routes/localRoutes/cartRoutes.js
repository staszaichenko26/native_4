import express from 'express'
import cartController from '../../controllers/cartController.js' 
import authMiddleware from '../../middleware/authMiddleware.js'
const router = express.Router()

router.post("/create", authMiddleware, cartController.cartCreate);
router.post("/addProducts", authMiddleware, cartController.addProducts);    
router.get("/getbyId", authMiddleware,cartController.getById);

export default router;