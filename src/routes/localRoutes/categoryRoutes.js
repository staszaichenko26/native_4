import express from 'express'
import categoryController from '../../controllers/categoryController.js'  
import authMiddleware from '../../middleware/authMiddleware.js'
const router = express.Router()

router.post("/create", categoryController.createCategory);    
router.delete("/:id", categoryController.deleteCategory);
router.post("/update/:id", categoryController.updateCategory);
router.get("/getbyId/:id", authMiddleware,  categoryController.getById);
router.get("/getAll", authMiddleware, categoryController.getAll);

export default router;